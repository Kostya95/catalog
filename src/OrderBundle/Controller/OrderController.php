<?php
/**
 * Created by PhpStorm.
 * User: kostya
 * Date: 20.01.17
 * Time: 22:36
 */

namespace OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OrderBundle\Entity\Orders;

/**
 * Class OrderController
 * @package OrderBundle\Controller
 */
class OrderController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@Order/Order/index.html.twig');
    }

    /**
     * Display order producs
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction()
    {
        $em = $this->getDoctrine();

        $user = $this->getUser();

        $products = $em->getRepository('OrderBundle:Orders')->getOrderProducts($user);
        
        return $this->render('@Order/Order/view.html.twig', ['products' => $products]);
    }

    /**
     * Order product
     * 
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction($name)
    {
        $user = $this->getUser();

        $userId = $user->getId();

        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository('ProductBundle:Product')->getOneProduct($name);

        $em->getRepository('OrderBundle:Orders')->createOrder($user, $product);

        return $this->redirectToRoute('product_list');
    }
}